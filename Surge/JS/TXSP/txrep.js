const url = $request.url;
const size = $response.headers["Content-Length"] || $response.headers["content-length"];
const transferEncoding = $response.headers["Transfer-Encoding"];

if (/^(https:\/\/i\.video|http:\/\/iacc)\.qq\.com\/$/.test(url)) {
  if (size < 1000) {
    $done();
  }
} else if (/^https:\/\/config\.ab\.qq\.com\/tab\/(GetTabToggle|GetTabRemoteConfig)$/.test(url)) {
  if (transferEncoding !== "chunked") {
    $done();
  }
}

$done({});