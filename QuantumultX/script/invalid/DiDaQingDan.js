/*
滴答清单 unlock pro
QX:
^https:\/\/(ticktick|dida365)\.com\/api\/v2\/user\/status url script-response-body https://raw.githubusercontent.com/Voldeemort/Surge/main/script/dida/dida.js

hostname = dida365.com, ticktick.com
*/
var body = $response.body;
var obj = JSON.parse(body);
obj.proEndDate = "2099-01-01T00:00:00.000+0000";
obj.needSubscribe = false;
obj.pro = true;
body = JSON.stringify(obj);
$done({body})