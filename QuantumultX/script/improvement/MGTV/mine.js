var obj = JSON.parse($response.body);
obj['data'] = {
    "reviewState" : 0,
    "list" : [
      {
        "data" : [
          {
            "id" : 10,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "",
            "moduleType" : 10,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "扫一扫",
            "image" : "https:\/\/ugc.hitv.com\/platform_oss\/96B12AC9D32E44FDA2F01BE188BB5C21.png",
            "requestUrl" : "",
            "rpt" : "id=10&pid=1&title=扫一扫",
            "jumpUrl" : "imgotv:\/\/sscan",
            "pid" : 1,
            "imageD" : "",
            "isPop" : 0
          },
          {
            "id" : 12,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "",
            "moduleType" : 18,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "消息",
            "image" : "https:\/\/ugc.hitv.com\/platform_oss\/CE91967590D6437B997C2EBB8C7FC2D7.png",
            "requestUrl" : "",
            "rpt" : "id=12&pid=1&title=消息",
            "jumpUrl" : "imgotv:\/\/smessage",
            "pid" : 1,
            "imageD" : "",
            "isPop" : 0
          },
          {
            "id" : 11,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "",
            "moduleType" : 10,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "搜索",
            "image" : "https:\/\/ugc.hitv.com\/platform_oss\/D8C1F312C895412FB04261E6684EFCD4.png",
            "requestUrl" : "",
            "rpt" : "id=11&pid=1&title=搜索",
            "jumpUrl" : "imgotv:\/\/search?keyword=",
            "pid" : 1,
            "imageD" : "",
            "isPop" : 0
          },
          {
            "id" : 13,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "",
            "moduleType" : 10,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "设置",
            "image" : "https:\/\/ugc.hitv.com\/platform_oss\/132DA3A6B6B94AC891E9611DD4AE9900.png",
            "requestUrl" : "",
            "rpt" : "id=13&pid=1&title=设置",
            "jumpUrl" : "imgotv:\/\/ssetings",
            "pid" : 1,
            "imageD" : "",
            "isPop" : 0
          }
        ],
        "id" : 1,
        "subTitle" : "",
        "moduleType" : 1,
        "title" : "顶部模块"
      },
      {
        "data" : [
          {
            "id" : 259,
            "subStyle" : {

            },
            "subTitle" : "领取芒果卡权益",
            "imageB" : "",
            "moduleType" : 104,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "领取芒果卡权益",
            "image" : "https://ossimg.hitv.com/platform_oss/8E70CF477CE84A9783967A56503ED978.png",
            "requestUrl" : "",
            "rpt" : "id=259&pid=2&title=领取芒果卡权益",
            "jumpUrl" : "imgotv://webview?url=https%3A%2F%2Fd.mgtv.com%2Fa-0Eld%3Fcxid%3Dmgcardclub0001%26activityId%3D7c3cb9013c0ff31f",
            "pid" : 2,
            "imageD" : "",
            "isPop" : 0
          },
          {
            "id" : 14,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "",
            "moduleType" : 11,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "用户信息",
            "image" : "https://avatar.hitv.com/upload/201806/08/wf_5b1a2a479768e.png",
            "requestUrl" : "https://homepage.bz.mgtv.com/v3/user/userInfo?artistId=3b3ad37b14f35c8587572ab3121b0ff6&uuid=3b3ad37b14f35c8587572ab3121b0ff6",
            "rpt" : "id=14&pid=2&title=用户信息",
            "jumpUrl" : "imgotv://noahprofile?uid=3b3ad37b14f35c8587572ab3121b0ff6",
            "pid" : 2,
            "imageD" : "",
            "isPop" : 0
          },
          {
            "id" : 15,
            "subStyle" : {

            },
            "subTitle" : "测试副标题",
            "imageB" : "",
            "moduleType" : 12,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "签到赢积分",
            "image" : "https://ugc.hitv.com/platform_oss/45083CE75D174B4EBE37D57E06AE8C2A.png",
            "requestUrl" : "https://credits.bz.mgtv.com/credits/url?uuid=3b3ad37b14f35c8587572ab3121b0ff6&sv=1",
            "rpt" : "id=15&pid=2&title=签到赢积分",
            "jumpUrl" : "imgotv://webview?url=https%3A%2F%2Fapp.mgtv.com%2Fcredits%2Findex.html%23%2F%3Ffrom%3Dtitle",
            "pid" : 2,
            "imageD" : "",
            "isPop" : 0
          }
        ],
        "id" : 2,
        "subTitle" : "",
        "moduleType" : 2,
        "title" : "用户信息模块"
      },
      {
        "data" : [
          {
            "id" : 21,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "",
            "moduleType" : 14,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "播放记录",
            "image" : "",
            "requestUrl" : "",
            "jumpUrl" : "imgotv://playhistory",
            "list" : [
              {
                "id" : 21,
                "subStyle" : {

                },
                "subTitle" : "",
                "imageB" : "",
                "moduleType" : 14,
                "popStyle" : {

                },
                "popTitle" : "",
                "title" : "播放记录",
                "image" : "",
                "requestUrl" : "",
                "rpt" : "id=21&pid=4&title=播放记录",
                "jumpUrl" : "imgotv://playhistory",
                "pid" : 4,
                "imageD" : "",
                "isPop" : 0
              }
            ],
            "pid" : 4,
            "imageD" : "",
            "isPop" : 0
          },
          {
            "popStyle" : {

            },
            "requestUrl" : "",
            "id" : 0,
            "title" : "追更助手",
            "moduleType" : 23,
            "subStyle" : {

            },
            "list" : [
              {
                "id" : 22,
                "subStyle" : {

                },
                "subTitle" : "我的下载",
                "imageB" : "https://ossimg.hitv.com/platform_oss/F3F00FD8919E453BA81C0ADE5B0721CF.png",
                "moduleType" : 15,
                "popStyle" : {

                },
                "popTitle" : "",
                "title" : "我的下载",
                "image" : "",
                "requestUrl" : "",
                "rpt" : "id=22&pid=4&title=我的下载",
                "jumpUrl" : "imgotv://offline",
                "pid" : 4,
                "imageD" : "https://ossimg.hitv.com/platform_oss/35506EA3E4204876A0322817EF352EAA.png",
                "isPop" : 0
              },
              {
                "id" : 227,
                "subStyle" : {

                },
                "subTitle" : "",
                "imageB" : "https://ossimg.hitv.com/platform_oss/57EB1977238B435CA4174F8881D9EB9D.png",
                "moduleType" : 10,
                "popStyle" : {

                },
                "popTitle" : "",
                "title" : "我的收藏",
                "image" : "",
                "requestUrl" : "",
                "rpt" : "id=227&pid=4&title=我的收藏",
                "jumpUrl" : "imgotv://collect_sub?position=0",
                "pid" : 4,
                "imageD" : "https://ossimg.hitv.com/platform_oss/58D8824ED04940D48F68558E6D7FEB82.png",
                "isPop" : 0
              },
              {
                "id" : 228,
                "subStyle" : {

                },
                "subTitle" : "",
                "imageB" : "https://ossimg.hitv.com/platform_oss/448CB5FAD57E4999ABFE652D76B3F7B8.png",
                "moduleType" : 10,
                "popStyle" : {

                },
                "popTitle" : "",
                "title" : "我的预约",
                "image" : "",
                "requestUrl" : "",
                "rpt" : "id=228&pid=4&title=我的预约",
                "jumpUrl" : "imgotv://collect_sub?position=1",
                "pid" : 4,
                "imageD" : "https://ossimg.hitv.com/platform_oss/613CD23CAF4D4896AF43EB1EECCB50BA.png",
                "isPop" : 0
              },
              {
                "id" : 229,
                "subStyle" : {

                },
                "subTitle" : "",
                "imageB" : "https://ossimg.hitv.com/platform_oss/52568ACC53F74D3393C352FB705312EA.png",
                "moduleType" : 10,
                "popStyle" : {

                },
                "popTitle" : "",
                "title" : "追更日历",
                "image" : "",
                "requestUrl" : "",
                "rpt" : "id=229&pid=4&title=追更日历",
                "jumpUrl" : "imgotv://nlist?pbId=1&tab=0",
                "pid" : 4,
                "imageD" : "https://ossimg.hitv.com/platform_oss/97F9CE369CC047AFA1ADC81700A6DB3E.png",
                "isPop" : 0
              }
            ],
            "isPop" : 0,
            "pid" : 0
          },
          {
            "popStyle" : {

            },
            "requestUrl" : "",
            "id" : 0,
            "title" : "追更banner",
            "moduleType" : 100,
            "subStyle" : {

            },
            "list" : [

            ],
            "isPop" : 0,
            "pid" : 0
          }
        ],
        "id" : 4,
        "subTitle" : "",
        "moduleType" : 4,
        "title" : "用户内容模块"
      },
      {
        "data" : [
          {
            "id" : 256,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "https://ossimg.hitv.com/platform_oss/06F8C8DB494C4C5FB8EA0B2AD5F8A777.png",
            "moduleType" : 16,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "我的音乐",
            "image" : "https://ossimg.hitv.com/platform_oss/72261D2906494422A9A74C31DDC4C799.png",
            "requestUrl" : "",
            "rpt" : "id=256&pid=7&title=我的音乐",
            "jumpUrl" : "imgotv://musicplaza?vclassId=100238",
            "pid" : 7,
            "imageD" : "https://ossimg.hitv.com/platform_oss/CEF3C54BD34B4F1CBD1F0BC95F1BE9C0.png",
            "isPop" : 0
          },
          {
            "id" : 31,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "https://ossimg.hitv.com/platform_oss/760AD0DE0CBF4E609BFEA99759F91FCA.png",
            "moduleType" : 16,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "我的资产",
            "image" : "https://ugc.hitv.com/platform_oss/4DF3C046C961438896500B49EF3ADE15.png",
            "requestUrl" : "",
            "rpt" : "id=31&pid=7&title=我的资产",
            "jumpUrl" : "imgotv://webview?url=https%3A%2F%2Fclub.mgtv.com%2Fact%2Fassets_index%2Findex.html",
            "pid" : 7,
            "imageD" : "https://ossimg.hitv.com/platform_oss/3F9C3376AF56439EB9CD4204FBD54C31.png",
            "isPop" : 0
          },
          {
            "id" : 233,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "https://ossimg.hitv.com/platform_oss/5468DF307FE74DF0BE2A38D9D727EB33.png",
            "moduleType" : 16,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "帮助与客服",
            "image" : "https://ossimg.hitv.com/platform_oss/2F9453A6CB5A43938E5F0EEFBB68937D.png",
            "requestUrl" : "",
            "rpt" : "id=233&pid=7&title=帮助与客服",
            "jumpUrl" : "imgotv://webview?url=https%3A%2F%2Fh5-kf.imgo.tv%2Ftemp.html%3Fapp_id%3D201%26platform%3D1%26system%3D3%26entry%3D4%26business_id%3D1%26pageName%3Dh5%26show_self_service%3D1%26show_questions%3D1%26show_faq%3D1",
            "pid" : 7,
            "imageD" : "https://ossimg.hitv.com/platform_oss/631BDA1BBEE1406A90B1F52B478D0A24.png",
            "isPop" : 0
          },
          {
            "id" : 34,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "https://ossimg.hitv.com/platform_oss/D31DE023DCB14043A54E93007B62E7F4.png",
            "moduleType" : 16,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "个性皮肤",
            "image" : "https://ugc.hitv.com/platform_oss/38946326B1A947AEAADB3B0B53C2FFC8.png",
            "requestUrl" : "",
            "rpt" : "id=34&pid=7&title=个性皮肤",
            "jumpUrl" : "imgotv://skinlist",
            "pid" : 7,
            "imageD" : "https://ossimg.hitv.com/platform_oss/742828D2B7574F839DA540FCC63F8791.png",
            "isPop" : 0
          },
          {
            "id" : 117,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "https://ossimg.hitv.com/platform_oss/18B2FFEFB2174A3193DCBA458531D1DE.png",
            "moduleType" : 10,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "功能实验室",
            "image" : "https://ugc.hitv.com/platform_oss/F98882830AAF472EA696798BBD419E79.png",
            "requestUrl" : "",
            "rpt" : "id=117&pid=7&title=功能实验室",
            "jumpUrl" : "imgotv://webview?url=https%3A%2F%2Fzl.mgtv.com%2Farticle%2F21090.html%3Fpre%3D1%26isHideNavBar%3D1",
            "pid" : 7,
            "imageD" : "https://ossimg.hitv.com/platform_oss/F066BD28E7B34E7F9598C620BC8054D6.png",
            "isPop" : 0
          },
          {
            "id" : 246,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "https://ossimg.hitv.com/platform_oss/A32B2EF9D97946D297131F3717E73EE5.png",
            "moduleType" : 16,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "意见反馈",
            "image" : "https://ossimg.hitv.com/platform_oss/8BE5BD7CB3BF4D089C58AB26C5AC23B3.png",
            "requestUrl" : "",
            "rpt" : "id=246&pid=7&title=意见反馈",
            "jumpUrl" : "imgotv://webview?url=https%3A%2F%2Fomgotv.mgtv.com%2Ffeedback",
            "pid" : 7,
            "imageD" : "https://ossimg.hitv.com/platform_oss/65BFCE4F61E441D3BD54BE08C65C1DC8.png",
            "isPop" : 0
          },
          {
            "id" : 255,
            "subStyle" : {

            },
            "subTitle" : "",
            "imageB" : "https://ossimg.hitv.com/platform_oss/BA69B075CA844F2A890D8836A3545366.png",
            "moduleType" : 10,
            "popStyle" : {

            },
            "popTitle" : "",
            "title" : "芒果壁纸",
            "image" : "https://ossimg.hitv.com/platform_oss/BB986B86545244768FC10EFD16E84519.png",
            "requestUrl" : "",
            "rpt" : "id=255&pid=7&title=芒果壁纸",
            "jumpUrl" : "imgotv://wallpaper",
            "pid" : 7,
            "imageD" : "https://ossimg.hitv.com/platform_oss/D63CB02123B7447FA9873241037DAE55.png",
            "isPop" : 0
          }
        ],
        "id" : 7,
        "subTitle" : "我的服务",
        "moduleType" : 7,
        "title" : "我的服务"
      }
    ]
  };
$done({ body: JSON.stringify(obj) });