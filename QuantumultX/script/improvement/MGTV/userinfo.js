var obj = JSON.parse($response.body);
obj['data']['expire']  = 2591985;
obj['data']['nickname']  = "财源滚滚";
obj['data']['vipExpiretime'] = 1719244799;
obj['data']['vipinfo'] = {
      "platform": "mpp",
      "isvip": 1,
      "vip_end_time": "2024-06-24 23:59:59",
      "type": "1",
      "vipdetail": [
        {
          "vip_id": "2",
          "vip_end_time": "2023-06-25 23:59:59",
          "bind_can_use": false,
          "isbind": false,
          "iscurrent": true,
          "type": "1"
        },
        {
          "vip_id": "2",
          "vip_end_time": "2024-06-24 23:59:59",
          "bind_can_use": false,
          "isbind": false,
          "iscurrent": false,
          "type": "1"
        }
      ],
      "growth": {
        "score": 9796,
        "level": 9
      },
      "ext": {
        "fingerprint": "0867343324cd4a75bd89daa9fe6bedfc",
        "first_recharge_time": "2023-01-08 20:08:08"
      }
    };
obj['data']['vipplatform'] = "mpp";
$done({ body: JSON.stringify(obj) });