{
  "config": {
    "immersive": 1,
    "serviceMap": {
      "profile_ext": "touid:5911211663"
    }
  },
  "channelInfo": {
    "channelConfig": {
      "channelType": "userProfile",
      "selectInfo": {
        "pageDataType": "flow",
        "tabKey": "weibo",
        "flowId": "1076035911211663"
      },
      "style": {
        "height": 44,
        "backgroundColor": "#FFFFFF",
        "backgroundDarkColor": "#FFFFFF"
      }
    },
    "channels": [
      {
        "pageDataType": "flow",
        "flowId": "2302835911211663",
        "containerid": "2302835911211663",
        "title": "Hot",
        "params": {
          "profile_check_value": "timestamp:1680194353|profileUid:5911211663|token:2fe26c4e",
          "profile_check_ext": "6012158854timestamp:1680194353|profileUid:5911211663",
          "profileTabType": "profile"
        },
        "apiPath": "\/2\/flowlist",
        "titleInfo": {
          "style": {
            "styleId": "channelTitleInfo",
            "font": "Normal",
            "selectFont": "Bold",
            "fontSize": 15,
            "selectFontSize": 15,
            "textColor": "#939393",
            "textDarkColor": "#888888",
            "selectTextColor": "#333333",
            "selectTextDarkColor": "#D3D3D3",
            "padding": [
              0,
              10,
              10,
              0
            ],
            "sliderColor": [
              "#FF6A00",
              "#FF6A00"
            ],
            "sliderDarkColor": [
              "#FF6A00",
              "#FF6A00"
            ],
            "iconSkinKey": "channelIcon_1",
            "textSkinKey": "channelText_1",
            "sliderIcon": "",
            "sliderIconDark": "",
            "sliderSkinKey": "channelSlider_1",
            "iconWidth": 24,
            "iconHeight": 24
          }
        },
        "theme": {
          "id": 1,
          "tabKey": "profile",
          "mustShow": 1,
          "hidden": 0
        }
      },
      {
        "pageDataType": "flow",
        "flowId": "1076035911211663",
        "containerid": "1076035911211663",
        "title": "Weibo",
        "params": {
          "profile_check_value": "timestamp:1680194353|profileUid:5911211663|token:2fe26c4e",
          "profile_check_ext": "6012158854timestamp:1680194353|profileUid:5911211663",
          "filterGroupStyle": 1,
          "profileTabType": "weibo"
        },
        "apiPath": "\/2\/profile\/container_timeline",
        "titleInfo": {
          "style": {
            "styleId": "channelTitleInfo",
            "font": "Normal",
            "selectFont": "Bold",
            "fontSize": 15,
            "selectFontSize": 15,
            "textColor": "#939393",
            "textDarkColor": "#888888",
            "selectTextColor": "#333333",
            "selectTextDarkColor": "#D3D3D3",
            "padding": [
              0,
              10,
              10,
              0
            ],
            "sliderColor": [
              "#FF6A00",
              "#FF6A00"
            ],
            "sliderDarkColor": [
              "#FF6A00",
              "#FF6A00"
            ],
            "backgroundIcon": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/04\/01\/weibotab.png",
            "selectBackgroundIcon": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/04\/01\/weibotab.png",
            "backgroundDarkIcon": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/04\/07\/weibotab_dark.png",
            "selectBackgroundDarkIcon": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/04\/07\/weibotab_dark.png",
            "iconWidth": 24,
            "iconHeight": 24,
            "iconSkinKey": "channelIcon_1",
            "textSkinKey": "channelText_1",
            "sliderIcon": "",
            "sliderIconDark": "",
            "sliderSkinKey": "channelSlider_1"
          }
        },
        "theme": {
          "id": 2,
          "tabKey": "weibo",
          "mustShow": 1,
          "hidden": 0
        },
        "serviceConfig": {
          "filter_group": {
            "filter_group_info": {
              "title": "All Weibo（91）",
              "icon": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2022\/05\/11\/shaixuan.png",
              "icon_name": "Filter",
              "icon_scheme": "",
              "search_scheme": "sinaweibo:\/\/searchall?profile_containerid=231802&type=401&trans_bg=1&disable_history=1&disable_sug=1&disable_hot=0&hint=Search in her Weibo&profile_uid=5911211663",
              "icon_code": 3059,
              "search_code": 3058
            },
            "filter_group": [
              {
                "name": "All Weibo",
                "containerid": "1076035911211663_-_WEIBO_SECOND_PROFILE_WEIBO",
                "scheme": "",
                "title": "All Weibo(91)",
                "actionLog": {
                  "act_code": 6112,
                  "ext": "scene:PROFILE_WEIBO|containerid:1076035911211663|ext:all",
                  "uicode": "10000198"
                }
              },
              {
                "name": "Original",
                "containerid": "1076035911211663_-_WEIBO_SECOND_PROFILE_WEIBO_ORI",
                "scheme": "",
                "title": "Original",
                "actionLog": {
                  "act_code": 6112,
                  "ext": "scene:PROFILE_WEIBO|containerid:1076035911211663|ext:ori",
                  "uicode": "10000198"
                }
              },
              {
                "name": "Filter by month",
                "containerid": "1076035911211663_-_WEIBO_SECOND_PROFILE_WEIBO_MONTH",
                "scheme": "sinaweibo:\/\/timefiltrate",
                "title": "Filter by month",
                "actionLog": {
                  "act_code": 6112,
                  "ext": "scene:PROFILE_WEIBO|containerid:1076035911211663|ext:time",
                  "uicode": "10000198"
                }
              }
            ]
          }
        }
      },
      {
        "pageDataType": "waterfall",
        "flowId": "2320925911211663_-_water_fall_index",
        "containerid": "2320925911211663_-_water_fall_index",
        "title": "Opus",
        "params": {
          "profile_check_value": "timestamp:1680194353|profileUid:5911211663|token:2fe26c4e",
          "profile_check_ext": "6012158854timestamp:1680194353|profileUid:5911211663",
          "profileTabType": "water_fall"
        },
        "apiPath": "\/2\/cardlist",
        "titleInfo": {
          "style": {
            "styleId": "channelTitleInfo",
            "font": "Normal",
            "selectFont": "Bold",
            "fontSize": 15,
            "selectFontSize": 15,
            "textColor": "#939393",
            "textDarkColor": "#888888",
            "selectTextColor": "#333333",
            "selectTextDarkColor": "#D3D3D3",
            "padding": [
              0,
              10,
              10,
              0
            ],
            "sliderColor": [
              "#FF6A00",
              "#FF6A00"
            ],
            "sliderDarkColor": [
              "#FF6A00",
              "#FF6A00"
            ],
            "backgroundIcon": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/08\/31\/videotab.png",
            "selectBackgroundIcon": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/08\/31\/videotab.png",
            "backgroundDarkIcon": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/08\/31\/videotab_dark.png",
            "selectBackgroundDarkIcon": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/08\/31\/videotab_dark.png",
            "iconWidth": 24,
            "iconHeight": 24,
            "iconSkinKey": "channelIcon_1",
            "textSkinKey": "channelText_1",
            "sliderIcon": "",
            "sliderIconDark": "",
            "sliderSkinKey": "channelSlider_1"
          }
        },
        "theme": {
          "id": 14,
          "tabKey": "mini_video",
          "mustShow": 1,
          "hidden": 0
        }
      },
      {
        "pageDataType": "flow",
        "flowId": "2318265911211663_-_mobile_profile_album_-_index",
        "containerid": "2318265911211663_-_mobile_profile_album_-_index",
        "title": "Album",
        "params": {
          "profile_check_value": "timestamp:1680194353|profileUid:5911211663|token:2fe26c4e",
          "profile_check_ext": "6012158854timestamp:1680194353|profileUid:5911211663",
          "profileTabType": "album"
        },
        "apiPath": "\/2\/flowlist",
        "titleInfo": {
          "style": {
            "styleId": "channelTitleInfo",
            "font": "Normal",
            "selectFont": "Bold",
            "fontSize": 15,
            "selectFontSize": 15,
            "textColor": "#939393",
            "textDarkColor": "#888888",
            "selectTextColor": "#333333",
            "selectTextDarkColor": "#D3D3D3",
            "padding": [
              0,
              10,
              10,
              0
            ],
            "sliderColor": [
              "#FF6A00",
              "#FF6A00"
            ],
            "sliderDarkColor": [
              "#FF6A00",
              "#FF6A00"
            ],
            "iconSkinKey": "channelIcon_1",
            "textSkinKey": "channelText_1",
            "sliderIcon": "",
            "sliderIconDark": "",
            "sliderSkinKey": "channelSlider_1",
            "iconWidth": 24,
            "iconHeight": 24
          }
        },
        "theme": {
          "id": 10,
          "tabKey": "album",
          "mustShow": 0,
          "hidden": 0
        }
      }
    ]
  },
  "footer": {
    "type": "userProfile",
    "data": {
      "toolbar_menus_new": {
        "style": {
          "padding": [
            0,
            13,
            0,
            13
          ],
          "backgroundUrl": "",
          "skinKey": "navigationBackground"
        },
        "items": [
          {
            "identifier": "message",
            "type": "toolbar_link",
            "status": "normal",
            "handlerType": "default_handler",
            "notificationname": null,
            "buttonMap": {
              "normal": {
                "disable": "0",
                "style": {
                  "type": "normal",
                  "padding": [
                    "0",
                    8,
                    "0",
                    8
                  ],
                  "widthRatio": "1",
                  "alignment": "1",
                  "space": "2",
                  "name": {
                    "skinKey": "messageNormalText",
                    "textColor": "#666666",
                    "textColorDark": "#999999",
                    "textFontSize": "10",
                    "text": "Message"
                  },
                  "image": {
                    "skinKey": "messageNormalIcon",
                    "width": "20",
                    "height": "20",
                    "pic": "https:\/\/h5.sinaimg.cn\/upload\/100\/888\/2021\/01\/19\/talk_default.png",
                    "pic_dark": "https:\/\/h5.sinaimg.cn\/upload\/100\/888\/2021\/01\/18\/talk-dark.png"
                  }
                },
                "param": {
                  "scheme": "sinaweibo:\/\/messagelist?uid=5911211663&nick=爱穿背带裤的小仙女&verified_type=-1&send_from=user_profile&ext={\"mark\":null,\"ext_biz_type\":3}",
                  "type": "vertical_arrange"
                },
                "actionlog": {
                  "act_code": "594",
                  "fid": "2307745911211663",
                  "oid": "messagelist",
                  "uicode": "10000198",
                  "cardid": "230774_-_WEIBO_INDEX_PROFILE_CHAT",
                  "ext": "uid:6012158854|ouid:5911211663|btn_name:Message|toolbar_type:1"
                }
              }
            }
          },
          {
            "identifier": "push_5911211663",
            "type": "toolbar_active",
            "status": "normal",
            "handlerType": "push_handler",
            "notificationname": "com.sina.weibo.blogger_push_update",
            "buttonMap": {
              "selected": {
                "style": {
                  "type": "normal",
                  "padding": [
                    "0",
                    8,
                    "0",
                    8
                  ],
                  "widthRatio": "1",
                  "alignment": "1",
                  "space": "2",
                  "name": {
                    "skinKey": "pushSelectedText",
                    "textColor": "#666666",
                    "textColorDark": "#999999",
                    "textFontSize": "10",
                    "text": "Subscribe"
                  },
                  "image": {
                    "skinKey": "pushSelectedIcon",
                    "width": "20",
                    "height": "20",
                    "pic": "https:\/\/h5.sinaimg.cn\/upload\/1014\/1355\/2022\/05\/13\/icon_tuisongtongzhi_default.png",
                    "pic_dark": "https:\/\/h5.sinaimg.cn\/upload\/1014\/1355\/2022\/05\/13\/icon_tuisongtongzhi_dark.png"
                  }
                },
                "param": {
                  "scheme": "sinaweibo:\/\/blogger_push?status=none&status_options=&blogger_uid=5911211663&blogger_gender=f&scene=profile"
                },
                "actionlog": {
                  "act_code": "594",
                  "fid": "2307745911211663",
                  "oid": "pushmanage",
                  "uicode": "10000198",
                  "cardid": "230774_-_WEIBO_INDEX_PROFILE_PUSH",
                  "ext": "uid:6012158854|ouid:5911211663|btn_name:Subscribe|btn_type:open|toolbar_type:1"
                }
              },
              "normal": {
                "disable": "0",
                "style": {
                  "type": "normal",
                  "padding": [
                    "0",
                    8,
                    "0",
                    8
                  ],
                  "widthRatio": "1",
                  "alignment": "1",
                  "space": "2",
                  "name": {
                    "skinKey": "pushNormalText",
                    "textColor": "#666666",
                    "textColorDark": "#999999",
                    "textFontSize": "10",
                    "text": "Subscribe"
                  },
                  "image": {
                    "skinKey": "pushNormalIcon",
                    "width": "20",
                    "height": "20",
                    "pic": "https:\/\/h5.sinaimg.cn\/upload\/1014\/1355\/2022\/05\/13\/icon_tuisongtongzhi_jia_default.png",
                    "pic_dark": "https:\/\/h5.sinaimg.cn\/upload\/1014\/1355\/2022\/05\/13\/icon_tuisongtongzhi_jia_dark.png"
                  }
                },
                "param": {
                  "scheme": "sinaweibo:\/\/blogger_push?status=none&status_options=&blogger_uid=5911211663&blogger_gender=f&scene=profile"
                },
                "actionlog": {
                  "act_code": "594",
                  "fid": "2307745911211663",
                  "oid": "pushmanage",
                  "uicode": "10000198",
                  "cardid": "230774_-_WEIBO_INDEX_PROFILE_PUSH",
                  "ext": "uid:6012158854|ouid:5911211663|btn_name:Subscribe|btn_type:close|toolbar_type:1"
                }
              }
            }
          },
          {
            "identifier": "follow",
            "type": "toolbar_follow",
            "status": "follow",
            "handlerType": "follow_handler",
            "notificationname": null,
            "buttonMap": {
              "follow": {
                "style": {
                  "padding": [
                    "0",
                    8,
                    "0",
                    8
                  ],
                  "type": "normal",
                  "widthRatio": "1",
                  "alignment": "1",
                  "space": "2",
                  "name": {
                    "skinKey": "followFollowText",
                    "textColor": "#666666",
                    "textColorDark": "#999999",
                    "textFontSize": "10",
                    "text": "Following"
                  },
                  "image": {
                    "skinKey": "followFollowIcon",
                    "width": "20",
                    "height": "20",
                    "pic": "https:\/\/h5.sinaimg.cn\/upload\/1014\/1355\/2022\/06\/22\/icon_yiguanzhu_default.png",
                    "pic_dark": "https:\/\/h5.sinaimg.cn\/upload\/1014\/1355\/2022\/06\/22\/icon_yiguanzhu_dark.png"
                  }
                },
                "param": {
                  "uid": "5911211663",
                  "type": "vertical_arrange",
                  "disable_group": 1,
                  "extparams": {
                    "updateToolBarMenu": 1
                  }
                },
                "actionlog": {
                  "act_code": "594",
                  "fid": "2307745911211663",
                  "oid": "follow",
                  "uicode": "10000198",
                  "cardid": "230774_-_WEIBO_INDEX_PROFILE_FOLLOW",
                  "ext": "uid:6012158854|ouid:5911211663|btn_name:Following|btn_type:following|toolbar_type:1"
                }
              },
              "specialFollow": {
                "style": {
                  "type": "normal",
                  "padding": [
                    "0",
                    8,
                    "0",
                    8
                  ],
                  "widthRatio": "1",
                  "alignment": "1",
                  "space": "2",
                  "name": {
                    "skinKey": "followSpecialFollowText",
                    "textColor": "#666666",
                    "textColorDark": "#999999",
                    "textFontSize": "10",
                    "text": "Special Follow"
                  },
                  "image": {
                    "skinKey": "followSpecialFollowIcon",
                    "width": "20",
                    "height": "20",
                    "pic": "https:\/\/h5.sinaimg.cn\/upload\/1014\/1355\/2022\/06\/22\/icon_tebieguanzhu_default.png",
                    "pic_dark": "https:\/\/h5.sinaimg.cn\/upload\/1014\/1355\/2022\/06\/22\/icon_tebieguanzhu_dark.png"
                  }
                },
                "param": {
                  "uid": "5911211663",
                  "type": "vertical_arrange",
                  "disable_group": 1,
                  "extparams": {
                    "updateToolBarMenu": 1
                  }
                },
                "actionlog": {
                  "act_code": "594",
                  "fid": "2307745911211663",
                  "oid": "specialFollow",
                  "uicode": "10000198",
                  "cardid": "230774_-_WEIBO_INDEX_PROFILE_FOLLOW",
                  "ext": "uid:6012158854|ouid:5911211663|btn_name:Special Follow|btn_type:specialfollow|toolbar_type:1"
                }
              }
            },
            "userinfo": {
              "id": 5911211663,
              "idstr": "5911211663",
              "screen_name": "爱穿背带裤的小仙女",
              "profile_image_url": "https:\/\/tvax2.sinaimg.cn\/crop.0.0.750.750.50\/006s2QbBly8h5uy1cheekj30ku0ku75b.jpg?KID=imgbed,tva&Expires=1680204498&ssig=vB6k5GfBd2",
              "following": false,
              "verified": false,
              "verified_type": -1,
              "remark": "",
              "avatar_large": "https:\/\/tvax2.sinaimg.cn\/crop.0.0.750.750.180\/006s2QbBly8h5uy1cheekj30ku0ku75b.jpg?KID=imgbed,tva&Expires=1680204498&ssig=51DGdEfShg",
              "avatar_hd": "https:\/\/tvax2.sinaimg.cn\/crop.0.0.750.750.1024\/006s2QbBly8h5uy1cheekj30ku0ku75b.jpg?KID=imgbed,tva&Expires=1680204498&ssig=4yLEKzj0hI",
              "follow_me": false,
              "mbtype": 2,
              "mbrank": 1,
              "level": 1,
              "type": 1,
              "story_read_state": -1,
              "special_follow": false,
              "allow_msg": 1,
              "friendships_relation": 2,
              "close_friends_type": 0
            },
            "menu_list": [
              {
                "type": "default",
                "name": "Speacial-follow",
                "default_type": "special_follow",
                "params": {
                  "action": "\/2\/friendships\/special_attention_create?btntype=1&uid=5911211663&profile_image=https%3A%2F%2Ftvax2.sinaimg.cn%2Fcrop.0.0.750.750.180%2F006s2QbBly8h5uy1cheekj30ku0ku75b.jpg%3FKID%3Dimgbed%2Ctva%26Expires%3D1680204498%26ssig%3D51DGdEfShg&updateToolBarMenu=1&urge_btn_enable=1"
                },
                "actionlog": {
                  "act_code": "2437",
                  "oid": "",
                  "fid": "2307745911211663",
                  "uicode": "10000198",
                  "cardid": "230774_-_WEIBO_INDEX_PROFILE_FOLLOW",
                  "uid": "6012158854",
                  "ouid": "5911211663",
                  "ext": "group:1"
                }
              },
              {
                "type": "link",
                "name": "Edit groups",
                "params": {
                  "scheme": "sinaweibo:\/\/selectgroup?containerid=2310182715099641_-_longbloglist&hideSpecialGoup=1&userid=5911211663",
                  "uid": "5911211663"
                },
                "actionlog": {
                  "act_code": "2437",
                  "oid": "",
                  "fid": "2307745911211663",
                  "uicode": "10000198",
                  "cardid": "230774_-_WEIBO_INDEX_PROFILE_FOLLOW",
                  "uid": "6012158854",
                  "ouid": "5911211663",
                  "ext": "group:2"
                }
              },
              {
                "type": "link",
                "name": "Set alias",
                "params": {
                  "scheme": "sinaweibo:\/\/remarkuser?userid=5911211663",
                  "uid": "5911211663"
                },
                "actionlog": {
                  "act_code": "2437",
                  "oid": "",
                  "fid": "2307745911211663",
                  "uicode": "10000198",
                  "cardid": "230774_-_WEIBO_INDEX_PROFILE_FOLLOW",
                  "uid": "6012158854",
                  "ouid": "5911211663",
                  "ext": "group:3"
                }
              },
              {
                "type": "follow",
                "name": "Unfollow",
                "unfollow_res": {
                  "title": "Unfollow"
                },
                "can_unfollow": 1,
                "relationship": 2,
                "params": {
                  "url": "sinaweibo:\/\/stopfollow?containerid=2310182715099641_-_longbloglist&userid=5911211663",
                  "uid": "5911211663",
                  "unfollow_in_profile": 1,
                  "extparams": {
                    "updateToolBarMenu": 1
                  }
                },
                "actionlog": {
                  "act_code": "2437",
                  "oid": "",
                  "fid": "2307745911211663",
                  "uicode": "10000198",
                  "cardid": "230774_-_WEIBO_INDEX_PROFILE_FOLLOW",
                  "uid": "6012158854",
                  "ouid": "5911211663",
                  "ext": "group:4"
                }
              }
            ]
          },
          {
            "identifier": "recommend",
            "type": "toolbar_active",
            "status": "normal",
            "handlerType": "recommend_handler",
            "notificationname": null,
            "buttonMap": {
              "normal": {
                "disable": "0",
                "style": {
                  "type": "normal",
                  "padding": [
                    "0",
                    8,
                    "0",
                    8
                  ],
                  "widthRatio": "1",
                  "alignment": "1",
                  "space": "2",
                  "name": {
                    "skinKey": "recommendNormalText",
                    "textColor": "#666666",
                    "textColorDark": "#999999",
                    "textFontSize": "10",
                    "text": "Recommend"
                  },
                  "image": {
                    "skinKey": "recommendNormalIcon",
                    "width": "20",
                    "height": "20",
                    "pic": "local_recommend",
                    "pic_dark": "local_recommend_dark"
                  }
                },
                "param": {
                  "scheme": "",
                  "uid": "5911211663"
                },
                "actionlog": {
                  "act_code": "594",
                  "fid": "2307745911211663",
                  "oid": "recommend",
                  "uicode": "10000198",
                  "cardid": "230774_-_WEIBO_INDEX_PROFILE_BTN_MORE",
                  "ext": "uid:6012158854|ouid:5911211663|btn_name:Recommend|toolbar_type:1"
                }
              }
            }
          }
        ]
      }
    }
  },
  "header": {
    "config": {
      "type": "userProfile"
    },
    "data": {
      "isVideoCoverStyle": false,
      "userInfo": {
        "id": 5911211663,
        "idstr": "5911211663",
        "screen_name": "爱穿背带裤的小仙女",
        "name": "爱穿背带裤的小仙女",
        "province": "32",
        "city": "1000",
        "description": "再也不忍俊不禁了",
        "url": "",
        "profile_image_url": "https:\/\/tvax2.sinaimg.cn\/crop.0.0.750.750.50\/006s2QbBly8h5uy1cheekj30ku0ku75b.jpg?KID=imgbed,tva&Expires=1680205153&ssig=LNHSHfhZjP",
        "light_ring": false,
        "cover_image_phone": "https:\/\/ww1.sinaimg.cn\/crop.0.0.640.640.640\/9d44112bjw1f1xl1c10tuj20hs0hs0tw.jpg",
        "domain": "",
        "weihao": "",
        "gender": "f",
        "followers_count": 66,
        "followers_count_str": "66",
        "friends_count": 474,
        "statuses_count": 91,
        "video_status_count": 1,
        "video_play_count": 0,
        "super_topic_not_syn_count": 0,
        "favourites_count": 4,
        "created_at": "Fri Apr 29 17:41:10 +0800 2016",
        "following": true,
        "verified": false,
        "verified_type": -1,
        "remark": "石",
        "email": "",
        "birthday": "1998-12-27",
        "constellation": "摩羯座",
        "insecurity": {
          "sexual_content": false
        },
        "status": {
          "visible": {
            "type": 0,
            "list_id": 0
          },
          "created_at": "Tue Dec 27 01:40:28 +0800 2022",
          "id": 4851102501047498,
          "idstr": "4851102501047498",
          "mid": "4851102501047498",
          "can_edit": false,
          "version": 1,
          "show_additional_indication": 0,
          "text": "http:\/\/t.cn\/A6KmhW5E",
          "textLength": 20,
          "source_allowclick": 0,
          "source_type": 1,
          "source": "<a href=\"http:\/\/app.weibo.com\/t\/feed\/1Nou1F\" rel=\"nofollow\">生日动态<\/a>",
          "appid": 811,
          "favorited": false,
          "pic_ids": [],
          "pic_types": "",
          "geo": null,
          "is_paid": false,
          "mblog_vip_type": 0,
          "annotations": [
            {
              "mapi_request": true
            }
          ],
          "reposts_count": 0,
          "comments_count": 0,
          "reprint_cmt_count": 0,
          "attitudes_count": 0,
          "pending_approval_count": 0,
          "isLongText": false,
          "multi_attitude": [
            {
              "type": 4,
              "count": 0
            },
            {
              "type": 5,
              "count": 0
            },
            {
              "type": 3,
              "count": 0
            },
            {
              "type": 2,
              "count": 0
            },
            {
              "type": 1,
              "count": 0
            }
          ],
          "reward_exhibition_type": 0,
          "hide_flag": 0,
          "mlevel": 0,
          "show_mlevel": 0,
          "biz_ids": [
            231601
          ],
          "hasActionTypeCard": 0,
          "hot_weibo_tags": [],
          "text_tag_tips": [],
          "mblogtype": 0,
          "rid": "0",
          "userType": 0,
          "more_info_type": 0,
          "positive_recom_flag": 0,
          "content_auth": 0,
          "gif_ids": "",
          "is_show_bulletin": 2,
          "safe_tags": 4294967296,
          "comment_manage_info": {
            "comment_permission_type": -1,
            "approval_comment_type": 0,
            "comment_sort_type": 0,
            "ai_play_picture_type": 0
          },
          "pic_num": 0,
          "can_reprint": false,
          "new_comment_style": 0
        },
        "ptype": 0,
        "avatar_large": "https:\/\/tvax2.sinaimg.cn\/crop.0.0.750.750.180\/006s2QbBly8h5uy1cheekj30ku0ku75b.jpg?KID=imgbed,tva&Expires=1680205153&ssig=VLocQhieRG",
        "avatar_hd": "https:\/\/tvax2.sinaimg.cn\/crop.0.0.750.750.1024\/006s2QbBly8h5uy1cheekj30ku0ku75b.jpg?KID=imgbed,tva&Expires=1680205153&ssig=x19oJ6gbQT",
        "verified_reason": "",
        "follow_me": false,
        "like": false,
        "like_me": false,
        "lang": "zh-cn",
        "star": 0,
        "mbtype": 2,
        "mbrank": 1,
        "svip": 0,
        "mb_expire_time": 1584719999,
        "level": 1,
        "type": 1,
        "user_limit": 0,
        "badge": {
          "uc_domain": 0,
          "enterprise": 0,
          "anniversary": 0,
          "taobao": 0,
          "gongyi": 0,
          "gongyi_level": 0,
          "bind_taobao": 0,
          "dailv": 0,
          "zongyiji": 0,
          "vip_activity1": 0,
          "unread_pool": 1,
          "daiyan": 0,
          "vip_activity2": 0,
          "fools_day_2016": 0,
          "uefa_euro_2016": 0,
          "unread_pool_ext": 1,
          "self_media": 0,
          "dzwbqlx_2016": 0,
          "discount_2016": 0,
          "follow_whitelist_video": 0,
          "league_badge": 0,
          "lol_msi_2017": 0,
          "super_star_2017": 0,
          "video_attention": 0,
          "travel_2017": 1,
          "lol_gm_2017": 0,
          "cz_wed_2017": 0,
          "inspector": 0,
          "panda": 0,
          "uve_icon": 0,
          "user_name_certificate": 1,
          "suishoupai_2018": 0,
          "wenda": 0,
          "wenchuan_10th": 0,
          "super_star_2018": 0,
          "worldcup_2018": 0,
          "wenda_v2": 0,
          "league_badge_2018": 0,
          "dailv_2018": 0,
          "asiad_2018": 0,
          "qixi_2018": 0,
          "yiqijuan_2018": 0,
          "meilizhongguo_2018": 0,
          "lol_s8": 0,
          "kpl_2018": 0,
          "national_day_2018": 0,
          "double11_2018": 0,
          "weibo_display_fans": 0,
          "relation_display": 0,
          "wbzy_2018": 0,
          "memorial_2018": 0,
          "v_influence_2018": 0,
          "hongbaofei_2019": 0,
          "status_visible": 0,
          "denglong_2019": 0,
          "fu_2019": 0,
          "womensday_2018": 0,
          "avengers_2019": 0,
          "suishoupai_2019": 0,
          "wusi_2019": 0,
          "earth_2019": 0,
          "hongrenjie_2019": 0,
          "dailv_2019": 0,
          "china_2019": 0,
          "hongkong_2019": 0,
          "jvhuasuan_2019": 0,
          "taohuayuan_2019": 0,
          "dzwbqlx_2019": 0,
          "rrgyj_2019": 0,
          "cishan_2019": 0,
          "family_2019": 0,
          "shouhuan_2019": 0,
          "ant_2019": 0,
          "weishi_2019": 0,
          "shuang11_2019": 0,
          "kdx_2019": 0,
          "wbzy_2019": 1,
          "starlight_2019": 0,
          "daqi_2019": 0,
          "gongjiri_2019": 0,
          "macao_2019": 0,
          "china_2019_2": 0,
          "hongbao_2020": 1,
          "feiyan_2020": 0,
          "hope_2020": 0,
          "kangyi_2020": 0,
          "daka_2020": 0,
          "green_2020": 0,
          "graduation_2020": 0,
          "pc_new": 7,
          "kfc_2020": 0,
          "dailv_2020": 0,
          "movie_2020": 0,
          "mi_2020": 0,
          "vpick_2020": 0,
          "cddyh_2020": 0,
          "nike_2020": 0,
          "school_2020": 0,
          "gongyi_2020": 0,
          "hongrenjie_2020": 0,
          "test_icon": 0,
          "china_2020": 0,
          "nissan_2020": 0,
          "zjszgf_2020": 0,
          "zaolang_2020": 0,
          "aizi_2020": 0,
          "wennuanji_2020": 0,
          "weibozhiye_2020": 0,
          "yijia7_2020": 0,
          "kfcflag_2021": 0,
          "hongbaofeifuniu_2021": 0,
          "cuccidlam20_2021": 0,
          "cuccidlam12_2021": 0,
          "cuccidlam25_2021": 0,
          "hongbaofeijika_2021": 1,
          "shequweiyuan_2021": 0,
          "weibozhiyexianxia_2021": 0,
          "zhongcaoguan_2021": 0,
          "nihaoshenghuojie_2021": 0,
          "lvzhilingyang_2021": 0,
          "xiaominewlogo_2021": 0,
          "disney5_2021": 0,
          "earthguarder_2021": 0,
          "yuanlongping_2021": 0,
          "ylpshuidao_2021": 0,
          "brand_account_2021": 0,
          "gaokao_2021": 0,
          "ouzhoubei_2021": 0,
          "biyeji_2021": 0,
          "party_cardid_state": 0,
          "hongrenjie_2021": 0,
          "aoyun_2021": 1,
          "zhongcaouser_2021": 0,
          "dailu_2021": 0,
          "companion_card": 0,
          "fishfarm_2021": 0,
          "kaixue21_2021": 0,
          "zhonghuacishanri_2021": 0,
          "renrengongyijie_2021": 0,
          "yinyuejie21_2021": 0,
          "qianbaofu_2021": 0,
          "yingxionglianmengs11_2021": 0,
          "yxlmlpl_2021": 0,
          "hongbaofei_2022": 0,
          "qichenqiche_2021": 0,
          "weibozhiye_2021": 0,
          "weibozhiyebobao_2021": 0,
          "social_content": 0,
          "hongbaofei2022_2021": 2,
          "dongaohui_2022": 0,
          "pc_experiment": 0,
          "youyic_2022": 0,
          "newdongaohui_2022": 0,
          "bddxrrdongaohui_2022": 0,
          "lvzhilingyang_2022": 0,
          "wenmingxiaobiaobing_2022": 0,
          "nihaochuntian_2022": 0,
          "video_visible": 0,
          "ceshiicon_2022": 0,
          "zuimeilaodongjie_2022": 0,
          "iplocationchange_2022": 1,
          "biyeji_2022": 0,
          "shuidao_2022": 0,
          "mengniu_2022": 0,
          "is_university": 0,
          "city_university": 0,
          "gaokao_2022": 0,
          "quanminjianshen_2022": 0,
          "hangmu_2022": 0,
          "guoqi_2022": 0,
          "gangqi_2022": 0,
          "dailv_2022": 0,
          "dailvmingxing_2022": 0,
          "comment_source": 0,
          "huoban_2022": 0,
          "zhongqiujie_2022": 0,
          "kaixueji_2022": 0,
          "renrengongyijie_2022": 0,
          "guoqing_2022": 0,
          "guoq_2022": 0,
          "s12_2022": 0,
          "clock_in_ug": 0,
          "hongrenjie_2022": 0,
          "pijingzhanji_2022": 0,
          "guangpanxingdong_2022": 0,
          "shijiebei_2022": 0,
          "hongrenjienew_2022": 0,
          "shijiebeigolden_2022": 0,
          "baokemeng_2022": 0,
          "moyudaka_2022": 0,
          "jiancjiyundong_2022": 0,
          "zhuijudaka_2022": 0,
          "shenyeshudongdaka_2022": 0,
          "suishoupaidaka_2022": 0,
          "meirimengchongdaka_2022": 0,
          "meirizaoqidaka_2022": 0,
          "meiriyicandaka_2022": 0,
          "ranghongbaofei_2023": 0,
          "pinganguo_2022": 0,
          "yuanshen_2023": 0,
          "chunjiesheyingdasai_2023": 0,
          "tuniandiyitiaoweibo_2023": 0,
          "xinyuncao_2023": 0,
          "taohua_2023": 0,
          "shangyeceshi1": 0,
          "shangyeceshi2": 0,
          "shangyeceshi3": 0,
          "shuimianri_2023": 0,
          "diqiuyixiaoshi_2023": 0,
          "star_crown": 0
        },
        "extend": {
          "privacy": {
            "mobile": 1
          },
          "mbprivilege": "0000000000000000000000000000000000000000000000000000000000000000"
        },
        "chaohua_ability": 0,
        "brand_ability": 0,
        "nft_ability": 0,
        "vplus_ability": 0,
        "wenda_ability": 0,
        "live_ability": 0,
        "gongyi_ability": 0,
        "paycolumn_ability": 0,
        "newbrand_ability": 0,
        "ecommerce_ability": 0,
        "hardfan_ability": 0,
        "wbcolumn_ability": 0,
        "interaction_user": 0,
        "credit_score": 80,
        "user_ability": 35914752,
        "urank": 9,
        "story_read_state": -1,
        "vclub_member": 0,
        "is_teenager": 0,
        "is_guardian": 0,
        "is_teenager_list": 0,
        "pc_new": 7,
        "special_follow": false,
        "planet_video": 2,
        "video_mark": 2,
        "live_status": 0,
        "user_ability_extend": 0,
        "status_total_counter": {
          "total_cnt": 72,
          "repost_cnt": 1,
          "comment_cnt": 40,
          "like_cnt": 31,
          "comment_like_cnt": 0
        },
        "video_total_counter": {
          "play_cnt": 255
        },
        "brand_account": 0,
        "hongbaofei": 0,
        "tab_manage": "[0, 0]",
        "green_mode": 0,
        "urisk": 0,
        "unfollowing_recom_switch": 1,
        "block": 2,
        "block_me": 2,
        "avatar_type": 0,
        "following_state": 1,
        "follow_me_state": 2,
        "counter_state": 0,
        "member_type": 2,
        "mask_type": 0,
        "allow_msg": 1,
        "icons": [
          {
            "name": "vip",
            "url": "https:\/\/h5.sinaimg.cn\/upload\/100\/1734\/2022\/06\/14\/open_vip.png",
            "isgif": false,
            "scheme": "sinaweibo:\/\/mppopupwindow?wbx_hide_close_btn=true&wbx_bg_view_dismiss=true&scheme=sinaweibo%3A%2F%2Fwbox%3Fid%3Dn1htatg0fm%26page%3Dpages%2Fcashier%2Fcashier%26cashier_id%3D98%26F%3DI_tq_zsbs_02_pop",
            "actionlog": {
              "act_code": 6858,
              "ext": "icon_scene:profile_icon|user_type:visitor|mbtype:2|vip_sign:open_vip.png|mbrank:1",
              "uicode": "10000198",
              "uid": "6012158854",
              "touid": 5911211663
            }
          }
        ],
        "avatar_extend_info": {
          "pendant_url_new": "",
          "pendant_scheme": "https:\/\/new.vip.weibo.cn\/headportrait\/mall?sinainternalbrowser=topnav&toolbar_hidden=1&bconf=3",
          "pendant_title": "设置头像挂件",
          "show_icon": true,
          "can_save_photo": true
        },
        "verified_scheme": "http:\/\/verified.weibo.com\/verify\/h5\/myverified?uid=5911211663",
        "description_scheme": "sinaweibo:\/\/cardlist?containerid=2302835911211663_-_INFO&title=%E5%9F%BA%E6%9C%AC%E4%BF%A1%E6%81%AF",
        "isGrayLikeUser": false,
        "cover_image_phone_level": 1,
        "covers": [
          {
            "pid": "70ace9b7ly1ggzusnypoej20yi0yiaop",
            "cover_thumbnails": "https:\/\/wx1.sinaimg.cn\/bmiddle\/70ace9b7ly1ggzusnypoej20yi0yiaop.jpg",
            "cover": "https:\/\/wx1.sinaimg.cn\/hworiginal\/70ace9b7ly1ggzusnypoej20yi0yiaop.jpg"
          }
        ],
        "orange_v": "",
        "tags": [],
        "ip_location": "",
        "friendships_relation": 2,
        "close_friends_type": 0,
        "follow_item_id": "0001980001_5911211663_4eb590ea"
      },
      "style": {
        "styleId": "headerDefalt",
        "avatar": {
          "size": 76,
          "v_size": 20,
          "border_width": 2,
          "boder_color": "#ffffff",
          "inside_border_width": 1,
          "inside_border_color": "#12000000",
          "mask_color": "#00000000",
          "mask_padding": "2",
          "boder_color_dark": "#00000000",
          "inside_border_color_dark": "#1affffff",
          "mask_color_dark": "#00000000",
          "border_color_dark": "#1E1E1E",
          "avatarBorderSkinKey": "avatarBorderColor"
        },
        "nick": {
          "verticalSpacing": 0,
          "text_color": "#333333",
          "text_color_dark": "#D3D3D3",
          "gender_top": 0,
          "membershop_top": -2,
          "grass_top": 0
        },
        "userinfo_offset": 33,
        "nick_offset": 31,
        "nick_offset_ios": 31,
        "heightScale": 0.5625
      },
      "userDescribe": [
        {
          "text": [
            {
              "type": "text",
              "style": {
                "styleId": "defaultNumberContent",
                "bold": "1",
                "textColor": "#333333",
                "textColorDark": "#D3D3D3",
                "textSize": 13
              },
              "content": "66"
            },
            {
              "type": "icon",
              "style": {
                "styleId": "defaultNumberIcon",
                "width": "4",
                "height": "5"
              }
            },
            {
              "type": "text",
              "style": {
                "styleId": "defaultNumberName",
                "textColor": "#939393",
                "textColorDark": "#888888",
                "textSize": 13
              },
              "content": "Followers"
            }
          ],
          "actionlog": {
            "act_code": 4644
          },
          "scheme": "https:\/\/m.weibo.cn\/c\/attention\/visit?showmenu=0&sign=5911211663&source=MQ==&role=NA=="
        },
        {
          "text": [
            {
              "type": "icon",
              "style": {
                "width": 20,
                "height": 5
              }
            }
          ]
        },
        {
          "text": [
            {
              "type": "text",
              "style": {
                "styleId": "defaultNumberContent",
                "bold": "1",
                "textColor": "#333333",
                "textColorDark": "#D3D3D3",
                "textSize": 13
              },
              "content": "474"
            },
            {
              "type": "icon",
              "style": {
                "styleId": "defaultNumberIcon",
                "width": "4",
                "height": "5"
              }
            },
            {
              "type": "text",
              "style": {
                "styleId": "defaultNumberName",
                "textColor": "#939393",
                "textColorDark": "#888888",
                "textSize": 13
              },
              "content": "Following"
            }
          ],
          "actionlog": {
            "act_code": 4643
          },
          "scheme": "https:\/\/m.weibo.cn\/c\/attention\/visit?showmenu=0&sign=5911211663&source=MA==&role=NA=="
        },
        {
          "text": [
            {
              "type": "icon",
              "style": {
                "width": 20,
                "height": 5
              }
            }
          ]
        },
        {
          "text": [
            {
              "type": "text",
              "style": {
                "styleId": "defaultNumberContent",
                "bold": "1",
                "textColor": "#333333",
                "textColorDark": "#D3D3D3",
                "textSize": 13
              },
              "content": "72"
            },
            {
              "type": "icon",
              "style": {
                "styleId": "defaultNumberIcon",
                "width": "4",
                "height": "5"
              }
            },
            {
              "type": "text",
              "style": {
                "styleId": "defaultNumberName",
                "textColor": "#939393",
                "textColorDark": "#888888",
                "textSize": 13
              },
              "content": "interaction"
            }
          ],
          "actionlog": {
            "act_code": 5859
          },
          "scheme": "sinaweibo:\/\/profileLikeDialog",
          "schemeData": {
            "title": [
              {
                "type": "text",
                "style": {
                  "bold": "true",
                  "textColor": "#333333",
                  "textColorKey": "CommonGray33",
                  "textSize": 17
                },
                "content": "Reposts&Comments&Likes"
              },
              {
                "type": "paragraph",
                "style": {
                  "alignment": 1
                }
              }
            ],
            "detail": [
              {
                "type": "text",
                "style": {
                  "textColor": "#939393",
                  "textColorKey": "CommonGray93",
                  "textSize": 12
                },
                "content": " 72 reposts,comments and likes in total"
              },
              {
                "type": "paragraph",
                "style": {
                  "alignment": 1
                }
              }
            ],
            "contents": [
              [
                {
                  "type": "icon",
                  "style": {
                    "width": 24,
                    "height": 24,
                    "darkMode": "alpha"
                  },
                  "iconUrl": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/06\/23\/repost.png"
                },
                {
                  "type": "icon",
                  "style": {
                    "width": 9,
                    "height": 25
                  },
                  "iconUrl": ""
                },
                {
                  "type": "text",
                  "style": {
                    "textColor": "#636363",
                    "textColorKey": "CommonGray63",
                    "textSize": 14
                  },
                  "content": "Reposts"
                },
                {
                  "type": "icon",
                  "style": {
                    "width": 10,
                    "height": 25
                  },
                  "iconUrl": ""
                },
                {
                  "type": "text",
                  "style": {
                    "bold": "true",
                    "textColor": "#333333",
                    "textColorKey": "CommonGray33",
                    "textSize": 14
                  },
                  "content": "1"
                }
              ],
              [
                {
                  "type": "icon",
                  "style": {
                    "width": 24,
                    "height": 24,
                    "darkMode": "alpha"
                  },
                  "iconUrl": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/06\/23\/comment.png"
                },
                {
                  "type": "icon",
                  "style": {
                    "width": 9,
                    "height": 25
                  },
                  "iconUrl": ""
                },
                {
                  "type": "text",
                  "style": {
                    "textColor": "#636363",
                    "textColorKey": "CommonGray63",
                    "textSize": 14
                  },
                  "content": "Comments"
                },
                {
                  "type": "icon",
                  "style": {
                    "width": 10,
                    "height": 25
                  },
                  "iconUrl": ""
                },
                {
                  "type": "text",
                  "style": {
                    "bold": "true",
                    "textColor": "#333333",
                    "textColorKey": "CommonGray33",
                    "textSize": 14
                  },
                  "content": "40"
                }
              ],
              [
                {
                  "type": "icon",
                  "style": {
                    "width": 24,
                    "height": 24,
                    "darkMode": "alpha"
                  },
                  "iconUrl": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/06\/23\/like.png"
                },
                {
                  "type": "icon",
                  "style": {
                    "width": 9,
                    "height": 25
                  },
                  "iconUrl": ""
                },
                {
                  "type": "text",
                  "style": {
                    "textColor": "#636363",
                    "textColorKey": "CommonGray63",
                    "textSize": 14
                  },
                  "content": "Likes"
                },
                {
                  "type": "icon",
                  "style": {
                    "width": 10,
                    "height": 25
                  },
                  "iconUrl": ""
                },
                {
                  "type": "text",
                  "style": {
                    "bold": "true",
                    "textColor": "#333333",
                    "textColorKey": "CommonGray33",
                    "textSize": 14
                  },
                  "content": "31"
                }
              ]
            ],
            "followlog": {
              "cardid": 2302830018
            }
          }
        }
      ],
      "tags": [
        {
          "title": "视频累计播放量255",
          "scheme": "",
          "color": "#FFFF6200",
          "color_dark": "#FFEA8011",
          "bg_color": "#59FFDDCB",
          "bg_color_dark": "#FF181818",
          "color_video_style": "#FFFC7011",
          "color_video_style_dark": "#FFDC6515",
          "bg_color_video_style": "#8C181818",
          "bg_color_video_style_dark": "#8C181818",
          "label_type": "video_play",
          "actionlog": {
            "act_code": 4629,
            "fid": "",
            "lfid": "profile_me",
            "uicode": "10000198",
            "luicode": "10000011",
            "oid": 5911211663,
            "cardid": "",
            "ext": "type:video_play"
          },
          "textSkinKey": "tagText_1",
          "backgroundSkinKey": "tagBackground_1"
        }
      ],
      "infoList": [
        {
          "icon": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2020\/05\/19\/description.png",
          "icon_dark": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2020\/05\/19\/description-dark.png",
          "icon_video_style": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/04\/16\/video_description.png",
          "icon_video_style_dark": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/04\/16\/video_description_dark.png",
          "scheme": "",
          "actionlog": {
            "act_code": 4630,
            "fid": "",
            "lfid": "profile_me",
            "uicode": "10000198",
            "luicode": "10000011",
            "oid": "",
            "cardid": "",
            "ext": "name:description"
          },
          "desc": "再也不忍俊不禁了",
          "textcolor": "#939393",
          "textcolor_dark": "#888888"
        },
        {
          "icon": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2020\/05\/19\/birth.png",
          "icon_dark": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2020\/05\/19\/birth-dark.png",
          "icon_video_style": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/04\/16\/video_birth.png",
          "icon_video_style_dark": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2021\/04\/16\/video_birth_dark.png",
          "scheme": "",
          "actionlog": "",
          "desc": "1998-12-27 摩羯座",
          "textcolor": "#939393",
          "textcolor_dark": "#888888"
        }
      ],
      "action_more": {
        "scheme": "sinaweibo:\/\/userdetailinfov2?uid=5911211663",
        "desc": "More Info",
        "textcolor": "#939393",
        "textcolor_dark": "#888888",
        "icon": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2022\/02\/28\/info_more.png",
        "icon_dark": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2022\/02\/28\/info_more_dark.png",
        "icon_width": 6.5,
        "icon_height": 12,
        "actionlog": {
          "act_code": 4631,
          "uicode": "10000198",
          "luicode": "10000011",
          "fid": "",
          "lfid": "profile_me",
          "oid": "",
          "cardid": "",
          "ext": ""
        }
      }
    }
  },
  "headerBack": {
    "config": {
      "type": "profileHeaderBack",
      "fixed": 0
    },
    "default": {
      "style": {
        "styleId": "headerBackDefault",
        "contentInset": [
          0,
          0,
          0,
          0
        ],
        "backgroundColor": "#EEEEEE",
        "backgroundColorDark": "#151515",
        "contentBottom": "ToChannelBarBottom",
        "height": 0,
        "contentModel": "scaleAspectTopFit",
        "heightScale": 0.5625,
        "backgroundImageSize": [
          100,
          100
        ]
      },
      "channelBarStyle": {
        "styleId": "headerBackChannelBar",
        "backgroundColor": "#FFFFFF",
        "backgroundDarkColor": "#1E1E1E",
        "font": "Normal",
        "selectFont": "Bold",
        "fontSize": 15,
        "selectFontSize": 15,
        "textColor": "#939393",
        "textDarkColor": "#888888",
        "selectTextColor": "#333333",
        "selectTextDarkColor": "#D3D3D3",
        "padding": [
          0,
          10,
          10,
          0
        ],
        "sliderColor": [
          "#FF6A00",
          "#FF6A00"
        ],
        "sliderDarkColor": [
          "#FF6A00",
          "#FF6A00"
        ]
      },
      "data": {
        "coverImage": [
          {
            "pid": "70ace9b7ly1ggzusnypoej20yi0yiaop",
            "cover_thumbnails": "https:\/\/wx1.sinaimg.cn\/bmiddle\/70ace9b7ly1ggzusnypoej20yi0yiaop.jpg",
            "cover": "https:\/\/wx1.sinaimg.cn\/hworiginal\/70ace9b7ly1ggzusnypoej20yi0yiaop.jpg"
          }
        ],
        "coverImageSkinKey": "coverImage",
        "coverVideo": {
          "skinKey": "coverVideo"
        },
        "backgroundUrl": "",
        "backgroundDarkUrl": "",
        "backgroundSkinKey": "headBackground",
        "maskFillColor": [
          "#00FFFFFF",
          "#FFFFFFFF"
        ],
        "maskFillColorDark": [
          "#001E1E1E",
          "#FF1E1E1E"
        ],
        "maskSkinKey": "headMask"
      }
    }
  },
  "navigationBar": {
    "type": "default",
    "data": {
      "title_bar_menus": [
        {
          "type": "search",
          "permanent": 1,
          "pic": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2022\/02\/25\/search_pic.png",
          "pic_highlight": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2022\/12\/08\/search_highlight.png",
          "scheme": "sinaweibo:\/\/searchall?profile_containerid=231802&type=401&disable_history=1&disable_sug=1&disable_hot=0&hint=Search in her Weibo&profile_uid=5911211663",
          "actionlog": {
            "act_code": 3058,
            "ext": "type:profileSearch"
          }
        },
        {
          "type": "share",
          "blocked": 0,
          "permanent": 1,
          "pic": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2022\/03\/30\/share_pic.png",
          "pic_highlight": "https:\/\/h5.sinaimg.cn\/upload\/1059\/799\/2022\/03\/30\/share_pic_hightlight.png",
          "shareData": {
            "title": "爱穿背带裤的小仙女",
            "desc": "再也不忍俊不禁了",
            "url": "https:\/\/m.weibo.cn\/u\/5911211663?",
            "icon": "https:\/\/tvax2.sinaimg.cn\/crop.0.0.750.750.180\/006s2QbBly8h5uy1cheekj30ku0ku75b.jpg?KID=imgbed,tva&Expires=1680205153&ssig=VLocQhieRG"
          }
        }
      ]
    }
  },
  "profileSkin": {
    "local": {
      "skinId": "",
      "md5": ""
    },
    "data": null
  }
}