var obj = JSON.parse($response.body);
if (obj.data.card) {
    obj['data']['card']['vip'] = {
        "vipStatusWarn": "",
        "vipType": 2,
        "dueRemark": "",
        "vipDueDate": 1964448000,
        "accessStatus": 0,
        "vipStatus": 1,
        "themeType": 0,
        "label": {
            "bg_color": "#FB7299",
            "bg_style": 1,
            "text": "十年大会员",
            "border_color": "",
            "path": "",
            "image" : "https://i0.hdslb.com/bfs/vip/8d7e624d13d3e134251e4174a7318c19a8edbd71.png",
            "label_theme": "ten_annual_vip",
            "text_color": "#FFFFFF"
        }
    };
    if (obj.data.card.official_verify) {
        obj['data']['card']['official_verify']['type'] = 1;
        obj['data']['card']['official_verify']['desc'] = "超级管理员";
        obj['data']['card']['official_verify']['role'] = 1;
        obj['data']['card']['official_verify']['title'] = "管理员";
    }
}
if (obj.data.images) {
    obj['data']['images']['imgUrl'] = "https://i0.hdslb.com/bfs/garb/item/bf3856cb87b786be6ca43813c0f73e31b27faab8.jpg";
    obj['data']['images']['show_reset'] = true;
    obj['data']['images']['night_imgurl'] = "";
    obj['data']['images']['garb'] = {
        "small_image" : "https://i0.hdslb.com/bfs/garb/item/bf3856cb87b786be6ca43813c0f73e31b27faab8.jpg",
        "image_id" : 1,
        "garb_id" : 4122,
        "large_image" : "https://i0.hdslb.com/bfs/garb/item/2961ae11f3f77f9086fd9702e2cb5b30e6f4d39f.jpg",
        "fans_label" : "早晚会发财",
        "fans_number" : "NO.000001"
      };
}
if (obj.data.ad_source_content_v2) {
    obj['data']['ad_source_content_v2']['is_ad_loc'] = false;
    obj['data']['ad_source_content_v2']['client_ip'] = "222.178.244.1";
}

$done({ body: JSON.stringify(obj) });
