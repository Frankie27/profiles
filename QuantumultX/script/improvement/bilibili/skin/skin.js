var obj = JSON.parse($response.body);
obj['data']['user_equip'] = {
      "package_url" : "https://i0.hdslb.com/bfs/garb/zip/9ad93f668673b8f6a6c68282d5a9353f8f921c21.zip",
      "id" : 35540,
      "preview" : "https://i0.hdslb.com/bfs/garb/item/ca7ac1993a0fd3987b84e1209cb99e6d53024ebf.jpg",
      "ver" : 1653545265,
      "data" : {
            "color": "#7f7f7f",
            "color_mode": "dark",
            "color_second_page": "#6e2122",
            "gray_rule": "true",
            "gray_rule_type": "all",
            "head_bg": "https://i0.hdslb.com/bfs/garb/item/c0cba58e7f30f52a32accfee767f1127cf22343f.jpg",
            "head_myself_mp4_bg": "https://i0.hdslb.com/bfs/garb/item/33b142bcd418246e7063b1e715852b4383a8a54a.mp4",
            "head_myself_mp4_play": "loop",
            "head_myself_squared_bg": "https://i0.hdslb.com/bfs/garb/item/78391596a1780fc1928e0a6c7eb93acd524d23d5.png",
            "head_tab_bg": "https://i0.hdslb.com/bfs/garb/item/aaa1a969cc0d7a56f0a4fbebd2f1b34c2f77d5bf.jpg",
            "image_cover": "https://i0.hdslb.com/bfs/garb/item/f16b1ce9dfb5b90b092a0b11ccf3fcaba8cd51d4.jpg",
            "image_preview": "https://i0.hdslb.com/bfs/garb/item/ca7ac1993a0fd3987b84e1209cb99e6d53024ebf.jpg",
            "package_md5": "12990bc353b0e215a5804ee13be18d2f",
            "package_url": "https://i0.hdslb.com/bfs/garb/zip/9ad93f668673b8f6a6c68282d5a9353f8f921c21.zip",
            "realname_auth": "false",
            "skin_mode": "normal",
            "tail_bg": "https://i0.hdslb.com/bfs/garb/item/2a61b33b770b71913356a2d21f2b9d2099c5a7fe.png",
            "tail_color": "#ffd076",
            "tail_color_selected": "#e7c9ff",
            "tail_icon_ani": "true",
            "tail_icon_ani_mode": "once",
            "tail_icon_channel": "https://i0.hdslb.com/bfs/garb/item/637048c16127e07fc74becd35a517b89260344af.png",
            "tail_icon_dynamic": "https://i0.hdslb.com/bfs/garb/item/fd2878ca4366c84d2801dc36e04bf02ad28eec65.png",
            "tail_icon_main": "https://i0.hdslb.com/bfs/garb/item/0583dc8e3e9a2856fb5b38f06cf4a01a94af1c68.png",
            "tail_icon_mode": "img",
            "tail_icon_myself": "https://i0.hdslb.com/bfs/garb/item/042f774923cd68e07e9cf3371f1ca283d19f4800.png",
            "tail_icon_pub_btn_bg": "https://i0.hdslb.com/bfs/garb/item/fd2878ca4366c84d2801dc36e04bf02ad28eec65.png",
            "tail_icon_selected_channel": "https://i0.hdslb.com/bfs/garb/item/fa15cb9480f1fbbb09c268146b0612c3aafcb2f8.png",
            "tail_icon_selected_dynamic": "https://i0.hdslb.com/bfs/garb/item/bb285cd5bae043411711b31f1cfde684c1242eac.png",
            "tail_icon_selected_main": "https://i0.hdslb.com/bfs/garb/item/8e25e69dc0e9bbff7aa47f0e25566d638ae5d08f.png",
            "tail_icon_selected_myself": "https://i0.hdslb.com/bfs/garb/item/704f290b6a95428e6efde7b63cc936ae234918c8.png",
            "tail_icon_selected_pub_btn_bg": "https://i0.hdslb.com/bfs/garb/item/bb285cd5bae043411711b31f1cfde684c1242eac.png",
            "tail_icon_selected_shop": "https://i0.hdslb.com/bfs/garb/item/9a8e4a5473572c3bc346242c5b71e16eafd61e42.png",
            "tail_icon_shop": "https://i0.hdslb.com/bfs/garb/item/b5c44c15be012bd08c4d5d93d9695b783288834b.png",
            "ver": "1653545265"
          },
      "name" : "穆小泠",
      "package_md5" : "12990bc353b0e215a5804ee13be18d2f"
    };
obj['data']['load_equip'] = {
      "loading_url" : "https://i0.hdslb.com/bfs/garb/item/1108a5917ebab5d6b256c40cc602a79970e27845.webp",
      "id" : 5298,
      "name" : "提摩西小队",
      "ver" : 1622443214
    };
$done({ body: JSON.stringify(obj) });
