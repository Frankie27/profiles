/*
# 忆飞Gif
# 测试版本v2.0
# @Marol62926

hostname = *.cloudfunctions.net

https://(us-central1-giftr-83d83|us-central1-infltr).cloudfunctions.net/verifySubscription url script-response-body https://raw.githubusercontent.com/Marol62926/MarScrpt/main/yifei.js

*/
var body = $response.body;
var obj = JSON.parse(body);

obj = {
  "result" : {
    "subscriptionTrialPeriod" : false,
    "subscriptionExpirationDateMs" : 1872518379000,
    "isEligibleForFreeTrial" : true,
    "subscriptionProductId" : "com.Yooshr.Giftr.subscriptionPremium.Yearly",
    "originalAppVersion" : "1205",
    "subscriptionAutoRenewStatus" : 1,
    "subscriber_platform" : "ios",
    "isValidSubscriber" : true,
    "isEligibleForSubscriptionOffer" : true,
    "subscriptionDateLastVerified" : 1651654341272,
    "subscriptionAutoRenewPreference" : "com.Yooshr.Giftr.subscriptionPremium.Yearly"
  }
}


body = JSON.stringify(obj);
$done({body});