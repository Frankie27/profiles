/*
# 经期助手 解锁 Premium
# 测试版本v8.2.1
# @Marol62926
# https://apps.apple.com/us/app/id1032267351

hostname = api.revenuecat.com

https://api.revenuecat.com/v1/(receipts|subscribers)/* url script-response-body https://raw.githubusercontent.com/Marol62926/MarScrpt/main/periodHelper.js


*/
var body = $response.body;
var obj = JSON.parse(body);

obj.subscriber.entitlements = {
      "Premium": {
        "expires_date": "2029-05-04T16:32:33Z",
        "grace_period_expires_date": null,
        "product_identifier": "com.lbrc.PeriodCalendar.premium.yearly",
        "purchase_date": "2022-05-01T16:32:33Z"
      }
  },
  
obj.subscriber.subscriptions ={
      "com.lbrc.PeriodCalendar.premium.yearly": {
        "billing_issues_detected_at": null,
        "expires_date": "2029-05-08T15:24:07Z",
        "grace_period_expires_date": null,
        "is_sandbox": false,
        "original_purchase_date": "2022-05-01T15:24:07Z",
        "ownership_type": "PURCHASED",
        "period_type": "trial",
        "purchase_date": "2022-05-01T15:24:07Z",
        "store": "app_store",
        "unsubscribe_detected_at": null
      }
  }

body = JSON.stringify(obj); 
$done(body); 