/*
# FILMR:视频剪辑 解锁 PRO
# 测试版本v7.6.18
# @Marol62926

下载地址：https://apps.apple.com/us/app/filmr-%E7%AE%80%E6%98%93%E8%A7%86%E9%A2%91%E7%BC%96%E8%BE%91%E5%99%A8/id1171358257?l=zh

hostname = payments.invideo.io

https://payments.invideo.io/verify_purchase url script-response-body https://raw.githubusercontent.com/Marol62926/MarScrpt/main/filmr.js
https://payments.invideo.io/subscription url script-response-body https://raw.githubusercontent.com/Marol62926/MarScrpt/main/filmr.js

*/
var body = $response.body; 
var obj = JSON.parse(body); 

obj = {
  "purchase_id": "480000515251249",
  "purchase_date": "2022-04-16T11:27:44Z",
  "next_renewal_date": "2029-04-19T11:27:43Z",
  "title": "FILMR PRO",
  "current_payment_source": "mobile",
  "show_next_renewal_date": true,
  "duration": "YEARLY",
  "plan": "FILMR_PRO",
  "upgrade_details": {
    "action_available": true,
    "action_title": "Upgrade to Desktop Plan",
    "upgradeable_plan_ids": [1]
  },
  "includes_access_to": ["Filmr Pro"],
  "is_iap_linked": true,
  "status": "ACTIVE",
  "is_free_user": false
}


body = JSON.stringify(obj);
$done(body);
