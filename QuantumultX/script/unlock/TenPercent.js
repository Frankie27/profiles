/*
# Ten Percent 解锁 Premium
# 测试版本v6.8.0
# @Marol62926

下载地址：https://apps.apple.com/us/app/ten-percent-happier-meditation/id992210239?l=zh

hostname = api.changecollective.com

https://api.changecollective.com/api/v3/user url script-response-body https://raw.githubusercontent.com/Marol62926/MarScrpt/main/tenPercent.js

*/
var body = $response.body;
var obj = JSON.parse(body);

obj.user.expiration_date = "05/05/2029, 05:13:20 PM UTC",
obj.user.plan_description = "1 year",
obj.user.subscription_end_date = "05/04/2029, 05:13:20 PM UTC",
obj.user.subscription_period = "P1Y",
obj.user.subscription_source = "62926",

$done({body: JSON.stringify(obj)});