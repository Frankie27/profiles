/*
# Adobe全家桶 解锁 Premium
# PS Express无法使用，其余未标出软件为Cloud Storage类型订阅
# @Marol62926

hostname = lcs-mobile-cops.adobe.io,createpdf.acrobat.com

# Adobe Photoshop, Adobe Illustrator, Adobe Lightroom, Premiere Rush, Adobe Express, Spark Page, Spark Video, Adobe Fresco
https://lcs-mobile-cops.adobe.io/mobile_profile/nul/v2 url script-response-body https://raw.githubusercontent.com/Marol62926/MarScrpt/main/adobe.js

# Adobe Scan, Adobe Acrobat
https://createpdf.acrobat.com/createpdf/api/users/me/subscriptions url script-response-body https://raw.githubusercontent.com/Marol62926/MarScrpt/main/adobeScanPdf.js

*/
var body = $response.body;
var obj = JSON.parse(body);

obj = {
  "subscriptions": [{
    "subscription_name": "AcrobatPlus",
    "subscription_level": "Plus",
    "subscription_state": "ACTIVE",
    "sub_ref": "",
    "biz_source": "CCC",
    "billing_term": null
  }, {
    "subscription_name": "ESign",
    "subscription_level": "Basic",
    "subscription_state": "ACTIVE",
    "sub_ref": "",
    "biz_source": "CCC",
    "billing_term": null
  }, {
    "subscription_name": "PDFPack",
    "subscription_level": "Plus",
    "subscription_state": "ACTIVE",
    "sub_ref": "",
    "biz_source": "",
    "billing_term": null
  }, {
    "subscription_name": "CreatePDF",
    "subscription_level": "Basic",
    "subscription_state": "ACTIVE",
    "sub_ref": "",
    "biz_source": "CCC",
    "billing_term": null
  }]
}
  
body = JSON.stringify(obj);
$done({body});