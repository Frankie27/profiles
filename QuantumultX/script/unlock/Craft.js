/*
Craft

#Craft(Eric)
^https:\/\/api\.craft\.do\/auth\/v2\/profile url script-response-body https://raw.githubusercontent.com/Alex0510/Eric/master/surge/Script/craft.js

api.craft.do
*/
let obj = JSON.parse($response.body);

obj.subscription = {"tier":"Pro","subscriptionActive":true,"expirationDate":4086377500000,"subscriptionType":"yearly","rawSubscriptionType": "AppStore","productId":"com.lukilabs.lukiapp.pro.sub.yearly"},
$done({body: JSON.stringify(obj)});
