/*
from https://t.me/rigouzhuanyehu

下载地址：https://apps.apple.com/us/app/big-mail/id1539128386?l=zh

Surge模块：
#!name=Big Mail
#!desc=

[Script]
Big Mail = type=http-response,pattern=^https:\/\/appstore\.getbigmail\.com\/subscriptions$,requires-body=1,max-size=0,script-path=Big Mail.js

[MITM]
hostname = %APPEND% appstore.getbigmail.com

Big Mail.js脚本内容：
let body = JSON.parse($response.body); 
body.isActive = true
body = JSON.stringify(body);
$done({body});

###################################
Quantumult X重写：
^https:\/\/appstore\.getbigmail\.com\/subscriptions$ url response-body "isActive":\w+ response-body "isActive":true

*/
let body = JSON.parse($response.body); 
body.isActive = true
body = JSON.stringify(body);
$done({body});