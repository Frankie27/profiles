/*
#!name=Financial Times (by litieyin)
#!desc=金融时报VIP&去开屏广告
^https:\/\/.*\.cloudfront\.net\/index.php\/jsapi\/paywall url script-response-body https://raw.githubusercontent.com/Redeembynight/own/main/QuantumultX/script/unlock/FT_chinese.js
^https:\/\/.*\.cloudfront\.net\/log\/new_log.php url reject
^https:\//creatives\.ftacademy\.cn\/ads url reject

hostname = *.cloudfront.net, creatives.ftacademy.cn
**************************/
var body = $response.body
    .replace(/.*/,"\{\"paywall\":0,\"premium\":1,\"standard\":1,\"addon\":0,\"expire\":\"4102415999\",\"v\":1994,\"campaign_code\":\"\",\"latest_duration\":\"yearly\"\}");
$done({ body });