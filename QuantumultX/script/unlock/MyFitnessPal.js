/*
# MyFitnessPal 解锁 Premium
# 测试版本v22.8.0
# @Marol62926

下载地址：https://apps.apple.com/us/app/myfitnesspal/id341232718?l=zh

hostname = premium-api.myfitnesspal.com

https://premium-api.myfitnesspal.com/v3/subscriptions/MFP/* url script-response-body https://raw.githubusercontent.com/Marol62926/MarScrpt/main/myFitnessPal.js

*/
var body = $response.body;
var obj = JSON.parse(body);

obj.subscriptionEndDate = "2029-05-01T10:10:53.929Z",
obj.subscriptionStartDate = "2022-05-01T10:10:53.929Z",
obj.hasPremium = true,
$done({body: JSON.stringify(obj)});