/*
# Lake: 涂色书解锁 Premium,邮箱随便输,不用验证
# 测试版本v3.24.0
# @Marol62926

hostname = revenuecat.lakecoloring.com

https://revenuecat.lakecoloring.com/v1/(receipts|subscribers) url script-response-body https://raw.githubusercontent.com/Marol62926/MarScrpt/main/lakecolor.js

*/
var body = $response.body;
var obj = JSON.parse(body);

obj.subscriber.entitlements = {
      "standard":{
              "expires_date":"2029-05-26T05:05:04Z",
              "product_identifier":"com.lake.coloring.sub.all1.promo2.yearly1",
              "purchase_date":"2022-04-09T05:05:04Z"
      }
  },
  
obj.subscriber.subscriptions ={
      "com.lake.coloring.sub.all1.promo2.yearly1":{
              "billing_issues_detected_at":null,
              "expires_date":"2029-05-26T05:05:04Z",
              "is_sandbox":false,
              "original_purchase_date":"2022-04-09T05:05:04Z",
              "period_type":"trial",
              "purchase_date":"2023-04-09T05:05:04Z",
              "store":"app_store",
              "unsubscribe_detected_at":null
      }
  }

  $done({body: JSON.stringify(obj)});