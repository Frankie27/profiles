/*
# AccuWeather 解锁 Premium
# 测试版本v15.2.1
# @Marol62926

下载地址：https://apps.apple.com/us/app/%E5%A4%A9%E6%B0%94%E9%A2%84%E6%8A%A5%E7%94%B1accuweather%E6%8F%90%E4%BE%9B/id300048137?l=zh

hostname = app-subscription-proxy.accuweather.com

https://app-subscription-proxy.accuweather.com/subscriptions/v1/apple/validate url script-response-body https://raw.githubusercontent.com/Marol62926/MarScrpt/main/accuWeather.js

*/
var body = $response.body;
var obj = JSON.parse(body);

obj = {
  "isValid": true,
  "autoRenewalStatus": null,
  "productId": "com.accuweather.annual.subscription",
  "expiryDateEpoch": 1871734859,
  "expirationDate": "2029-05-01T14:18:35+00:00",
  "expirationIntent": null,
  "expirationRetry": null,
  "purchaseDateEpoch": 1650809915000,
  "isTrial": false,
  "isIntro": true,
  "status": 0,
  "usedTrial": ["com.accuweather.annual.subscription"],
  "usedIntro": null,
  "isRetryable": null
}
  

body = JSON.stringify(obj);
$done({body});