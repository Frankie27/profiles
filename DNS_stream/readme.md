一.安装dnsmasq

apt-get -y install dnsmasq
二.配置dnsmasq

1.配置文件/etc/dnsmasq.conf

vi /etc/dnsmasq.conf

server=/netflix.com/4.4.4.4
server=/disneyplus.com/4.4.4.4
...
...
resolv-file=/etc/resolv.dnsmasq.conf

具体需要添加的域名请在TG群发送“域名规则”获取。4.4.4.4以实际DNS为准。
2.配置文件/etc/resolv.dnsmasq.conf

vi /etc/resolv.dnsmasq.conf

nameserver 1.1.1.1
nameserver 8.8.8.8
3.设置VPS系统DNS，将本机dnsmasq作为系统DNS服务器。参考上面的操作

 chattr -i /etc/resolv.conf
 echo -e "nameserver 127.0.0.1" > /etc/resolv.conf
 chattr +i /etc/resolv.conf      
4.重启dnsmasq

/etc/init.d/dnsmasq restart
四.卸载

apt-get remove dnsmasq
chattr -i /etc/resolv.conf
chmod 777 /etc/resolv.conf
echo -e "nameserver 8.8.8.8" > /etc/resolv.conf