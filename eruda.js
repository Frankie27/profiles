let modifiedHeaders = { ...$response.headers };
if (modifiedHeaders['Content-Security-Policy']) {
    delete modifiedHeaders['Content-Security-Policy'];
}
let html = $response.body;
// 定义要插入的代码
let sToInsert = '<script src="https://cdn.jsdelivr.net/npm/eruda"></script><script>eruda.init();</script>';
// 判断是否存在 <head> 标签
if (html.includes('</head>') != -1) {
// 插入eruda
    html = sToInsert + html ;
}
$done({ body: html, headers: modifiedHeaders });